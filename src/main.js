import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
import './index.css'
import VueWriter from "vue-writer"
import VueLoading from "vue-loading-overlay"
import VueKinesis from "vue-kinesis";
import Particles from "particles.vue3";
import LoadScript from "vue-plugin-load-script";





const app = createApp(App)

app.use(VueLoading)
app.use(router)
app.use(VueWriter)
app.use(VueKinesis)
app.use(Particles)
app.use(LoadScript);
app.mount('#app')

