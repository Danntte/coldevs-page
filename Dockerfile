FROM node:lts-alpine as build-stage
#-g instalacion global 
RUN npm install -g http-server 
#creating app directory

#setting /app carpet as a workdirectory
WORKDIR /app
#copying all related to package.jason.  the ./ is to copy it into the workdirectory.  * is to coppy all package and package-lock
COPY package*.json ./
#gsap library (this is only for coldevs page)
COPY gsap-bonus.tgz ./

RUN npm install
#COPY ALL archives into the app carpet
COPY . .

RUN npm run build

FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
#expose the ports from alpine(container) (not local machine)
EXPOSE 80
#execute http-server dist in alpine 
CMD ["nginx", "-g", "daemon off;"]
