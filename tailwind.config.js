module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {

      textUnderlineOffset :{
        20: '20px'
      },
      with:{
        '104': '104px'
      },
      height:{
        '60': '60px'
      },
      backgroundImage: {
        'home-pattern': "url('public/backgroundHome.png')",
        'bgProjects1' : "url('./src/components/imgProjects/bgProjects1.jpg')",
        'bgManu': "url('./src/components/imgTeam/bgManu.png')",
        'bgAboutUs' : "url('public/backgrundAboutUs.png')",
        'bg-gif-pattern' : "url('public/backgrund.gif')",
      }
    },
    colors: {
      transparent: 'transparent',
      'cd-blue': '#05299E',
      'cd-blue2': '#0567A8',
      'cd-blue3': '#0071BC',
      'cd-blue4': '#1F84A3',
      'white': 'white',
      'black': 'black',
    },
    fontFamily: {
      'Montserrat' : ['"Montserrat"' ,'sans-serif'],
    },
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1536px',
      'fhd': '1920px'
    },
  },
  plugins: [],
}